﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatrExample.Core.Core;

namespace MediatrExample.Shared
{
    public class SimpleMediationContext : IMediationContext
    {
        public List<string> SharedConf { get; set; }
    }
}
