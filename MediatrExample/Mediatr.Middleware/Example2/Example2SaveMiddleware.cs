﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Core.Middleware;
using MediatrExample.Interfaces;
using MediatrExample.Mediatr.Attribute;
using MediatrExample.Mediatr.Queries;
using MediatrExample.Mediatr.Response;

namespace MediatrExample.Mediatr.Middleware.Example2
{
    [Example2Order(2)]
    public class Example2Save : IMiddleware<Example2Query, ExampleResponse>
    {
        private readonly IServiceA _logger;

        public Example2Save(IServiceA logger)
        {
            _logger = logger;
        }
     
        public async Task<ExampleResponse> RunAsync(Example2Query request, IMediationContext mediationContext, CancellationToken cancellationToken,
            HandleRequestDelegate<Example2Query, ExampleResponse> next)
        {
            Console.WriteLine($"{request.ValueA} - {request.ValueB}");

            return await next.Invoke(request, mediationContext, cancellationToken);
        }
    }
}
