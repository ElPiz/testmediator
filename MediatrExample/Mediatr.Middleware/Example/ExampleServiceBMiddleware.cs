﻿using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Core.Middleware;
using MediatrExample.Interfaces;
using MediatrExample.Mediatr.Attribute;
using MediatrExample.Mediatr.Queries;
using MediatrExample.Mediatr.Response;

namespace MediatrExample.Mediatr.Middleware.Example
{
    [ExampleOrder(3)]
    public class ExampleServiceB : IMiddleware<ExampleQuery, ExampleResponse>
    {
        private readonly IServiceB _service;

        public ExampleServiceB(IServiceB service)
        {
            _service = service;
        }
        public async Task<ExampleResponse> RunAsync(ExampleQuery request, IMediationContext mediationContext,
            CancellationToken cancellationToken, HandleRequestDelegate<ExampleQuery, ExampleResponse> next)
        {
            request.ValueB = _service.Value;
            return await next.Invoke(request, mediationContext, cancellationToken);
        }
    }
}
