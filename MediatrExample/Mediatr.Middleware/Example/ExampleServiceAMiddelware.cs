﻿using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Core.Middleware;
using MediatrExample.Interfaces;
using MediatrExample.Mediatr.Attribute;
using MediatrExample.Mediatr.Queries;
using MediatrExample.Mediatr.Response;

namespace MediatrExample.Mediatr.Middleware.Example
{
    [ExampleOrder(4)]
    public class ExampleServiceA : IMiddleware<ExampleQuery, ExampleResponse>
    {
        private readonly IServiceA _service;

        public ExampleServiceA(IServiceA service)
        {
            _service = service;
        }

        public async Task<ExampleResponse> RunAsync(ExampleQuery request, IMediationContext mediationContext,
            CancellationToken cancellationToken, HandleRequestDelegate<ExampleQuery, ExampleResponse> next)
        {
            request.ValueA = _service.Value;
            return await next.Invoke(request, mediationContext, cancellationToken);
        }
    }
}
