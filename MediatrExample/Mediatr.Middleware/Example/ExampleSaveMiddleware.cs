﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Core.Middleware;
using MediatrExample.Interfaces;
using MediatrExample.Mediatr.Attribute;
using MediatrExample.Mediatr.Queries;
using MediatrExample.Mediatr.Response;

namespace MediatrExample.Mediatr.Middleware.Example
{
    [ExampleOrder(5)]
    public class ExampleSave : IMiddleware<ExampleQuery, ExampleResponse>, IMiddleware<Example2Query, ExampleResponse>
    {
        private readonly IServiceA _logger;

        public ExampleSave(IServiceA logger)
        {
            _logger = logger;
        }
        public async Task<ExampleResponse> RunAsync(ExampleQuery request, IMediationContext mediationContext,
            CancellationToken cancellationToken, HandleRequestDelegate<ExampleQuery, ExampleResponse> next)
        {
            Console.WriteLine($"{request.ValueA} - {request.ValueB}");
            
            return await next.Invoke(request, mediationContext, cancellationToken);
        }

        public async Task<ExampleResponse> RunAsync(Example2Query request, IMediationContext mediationContext, CancellationToken cancellationToken, HandleRequestDelegate<Example2Query, ExampleResponse> next)
        {
            Console.WriteLine($"{request.ValueA} - {request.ValueB}");

            return await next.Invoke(request, mediationContext, cancellationToken);
        }
    }
}
