﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Core.Middleware;
using MediatrExample.Mediatr.Attribute;
using MediatrExample.Mediatr.Queries;
using MediatrExample.Mediatr.Response;

namespace MediatrExample.Mediatr.Middleware.Example
{
    [ExampleOrder(2)]
    public class ExampleValidator : IMiddleware<ExampleQuery, ExampleResponse>
    {
        public async Task<ExampleResponse> RunAsync(ExampleQuery request, IMediationContext mediationContext,
            CancellationToken cancellationToken, HandleRequestDelegate<ExampleQuery, ExampleResponse> next)
        {
            Console.WriteLine("Validation hit");
            return await next.Invoke(request, mediationContext, cancellationToken);
        }
    }
}
