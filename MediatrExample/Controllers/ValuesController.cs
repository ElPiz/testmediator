﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Mediatr.Commands;
using MediatrExample.Mediatr.Events;
using MediatrExample.Mediatr.Queries;
using MediatrExample.Mediatr.SharedContext;
using Microsoft.AspNetCore.Mvc;

namespace MediatrExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private IMediator _mediator;

        public ValuesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> Get()
        {
            var context = new SimpleMediationContext();



            var exampleQuery = new ExampleQuery();
            var simpleEvent = new ExampleEvent();

            var result = await _mediator.HandleAsync(exampleQuery, context);


            simpleEvent.Result = result.Values.FirstOrDefault();

            await _mediator.HandleAsync(simpleEvent);
            
            
            //Example 2
            var example2Query = new Example2Query();
            var result2 = await _mediator.HandleAsync(example2Query, context);



            return result2.Values;

        }
    }
}
