﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediatrExample.Mediatr.Response
{
    public class ExampleResponse : ValidationResult
    {
        public List<string> Values { get; set; }
    }
}
