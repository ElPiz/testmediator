﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;

namespace MediatrExample.Mediatr.Events
{
    public class ExampleSaveDbEventHandler : Core.Event.EventHandler<ExampleEvent>
    {
        protected override async Task HandleEventAsync(ExampleEvent @event, IMediationContext mediationContext,
            CancellationToken cancellationToken)
        {
            if(@event?.Result == "values1")
                Console.WriteLine("Event handler 1");
        }
    }
}
