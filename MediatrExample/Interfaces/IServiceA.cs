﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediatrExample.Interfaces
{
    public interface IServiceA
    {
        string Value { get; set; }
    }
}
