﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatrExample.Core.Core;

namespace MediatrExample.Mediatr.Attribute
{
    
    public class ExampleOrderAttribute : DisplayOrderAttribute
    {
        public ExampleOrderAttribute(int order) : base(order)
        {
        }
    }
}
