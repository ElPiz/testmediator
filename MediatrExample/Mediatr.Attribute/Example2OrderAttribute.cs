﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatrExample.Core.Core;

namespace MediatrExample.Mediatr.Attribute
{
    public class Example2OrderAttribute : DisplayOrderAttribute
    {
        public Example2OrderAttribute(int order) : base(order)
        {
        }
    }
}
