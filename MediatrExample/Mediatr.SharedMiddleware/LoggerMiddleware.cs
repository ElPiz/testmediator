﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Core.Middleware;
using MediatrExample.Mediatr.Attribute;

namespace MediatrExample.Mediatr.SharedMiddleware
{
    [ExampleOrder(1)]
    [Example2Order(3)]
    public class LoggerMiddleware<TRequest, TResponse> : IMiddleware<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        public async Task<TResponse> RunAsync(TRequest request, IMediationContext mediationContext,
            CancellationToken cancellationToken, HandleRequestDelegate<TRequest, TResponse> next)
        {
            Console.WriteLine("Request pre logged using middleware 1");
            var result = await next.Invoke(request, mediationContext, cancellationToken);
            Console.WriteLine("Request post logged using middleware 1");

            return result;
        }
    }
}
