﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatrExample.Core.Queries;
using MediatrExample.Mediatr.Attribute;
using MediatrExample.Mediatr.Response;

namespace MediatrExample.Mediatr.Queries
{
    
    public class ExampleQuery : IQuery<ExampleResponse>
    {
        public string ValueA { get; set; }
        public string ValueB { get; set; }

    }
}
