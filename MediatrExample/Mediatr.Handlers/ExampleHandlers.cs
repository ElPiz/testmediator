﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Core.Queries;
using MediatrExample.Mediatr.Attribute;
using MediatrExample.Mediatr.Queries;
using MediatrExample.Mediatr.Response;

namespace MediatrExample.Mediatr.Handlers
{
    [ExampleOrder(0)]
    public class ExampleHandlers : QueryHandler<ExampleQuery, ExampleResponse>
    {
        protected override async Task<ExampleResponse> HandleQueryAsync(ExampleQuery query,
            IMediationContext mediationContext, CancellationToken cancellationToken)
        {
            List<string> Populate(List<string> list)
            {
                list.Add(query.ValueA);
                list.Add(query.ValueB);
                return list;
            }
           
            return new ExampleResponse()
            {
                Values = Populate(new List<string>())
            };
        }
    }
}
