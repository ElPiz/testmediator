﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;
using MediatrExample.Core.Queries;
using MediatrExample.Mediatr.Attribute;
using MediatrExample.Mediatr.Queries;
using MediatrExample.Mediatr.Response;

namespace MediatrExample.Mediatr.Handlers
{
    [Example2Order(0)]
    public class Example2Handlers : QueryHandler<Example2Query, ExampleResponse>
    {
        protected override async Task<ExampleResponse> HandleQueryAsync(Example2Query query,
            IMediationContext mediationContext, CancellationToken cancellationToken)
        {
            var response = new ExampleResponse();
            response.Values = new List<string>(){ "Value1Example2"};


            return response;
        }
    }
}
