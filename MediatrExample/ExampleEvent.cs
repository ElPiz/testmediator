﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatrExample.Core.Event;

namespace MediatrExample
{
    public class ExampleEvent :IEvent
    {
        public string Result { get; set; }
    }
}
