﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatrExample.Core.Core;

namespace MediatrExample.Mediatr.SharedContext
{
    public class SimpleMediationContext : IMediationContext
    {
        public List<string> SharedConf { get; set; }
    }
}
