﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;

namespace MediatrExample.Core.Middleware
{
    public delegate Task<TResponse> HandleRequestDelegate<in TRequest, TResponse>(TRequest request, IMediationContext mediationContext, CancellationToken cancellationToken);

    public interface IMiddleware<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        Task<TResponse> RunAsync(TRequest request, IMediationContext mediationContext,
            CancellationToken cancellationToken, HandleRequestDelegate<TRequest, TResponse> next);
    }
}
