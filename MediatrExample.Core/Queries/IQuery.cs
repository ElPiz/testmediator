﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatrExample.Core.Core;

namespace MediatrExample.Core.Queries
{
    public interface IQuery<TResult> : IRequest<TResult>
    {
    }
}
