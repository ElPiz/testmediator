﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatrExample.Core.Core;

namespace MediatrExample.Core.Commands
{
    public interface ICommandHandler<in TCommand> : IRequestHandler<TCommand, Unit> where TCommand : IRequest<Unit>
    {
    }
}
