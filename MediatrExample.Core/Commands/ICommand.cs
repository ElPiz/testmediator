﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatrExample.Core.Core;

namespace MediatrExample.Core.Commands
{
    public interface ICommand : IRequest<Unit>
    {
    }
}
