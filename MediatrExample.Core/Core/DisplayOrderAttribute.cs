﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediatrExample.Core.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public abstract class DisplayOrderAttribute : Attribute
    {
        private int order;

        protected DisplayOrderAttribute(int order)
        {
            this.order = order;
        }

        public virtual int Order => order;
    }
}
