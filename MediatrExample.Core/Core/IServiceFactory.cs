﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediatrExample.Core.Core
{

    public delegate object ServiceFactoryDelegate(Type type);

    public interface IServiceFactory
    {
        object GetInstance(Type T);
    }
}
