﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatrExample.Core.Core;

namespace MediatrExample.Core.Event
{
    public interface IEvent : IRequest<Unit>
    {

    }
}
