﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatrExample.Core.Core;

namespace MediatrExample.Core.Event
{
    public abstract class EventHandler<TEvent> : IEventHandler<TEvent> where TEvent : IRequest<Unit>
    {
        public async Task<Unit> HandleAsync(TEvent request, IMediationContext mediationContext,
            CancellationToken cancellationToken)
        {
            await HandleEventAsync(request, mediationContext, cancellationToken);
            return Unit.Result;
        }

        protected abstract Task HandleEventAsync(TEvent @event, IMediationContext mediationContext,
            CancellationToken cancellationToken);
    }
}
