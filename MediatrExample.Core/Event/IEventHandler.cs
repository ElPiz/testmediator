﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatrExample.Core.Core;

namespace MediatrExample.Core.Event
{
    public interface IEventHandler<in TEvent> : IRequestHandler<TEvent, Unit> where TEvent : IRequest<Unit>
    {
    }
}
